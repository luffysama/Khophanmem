﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

public partial class _Default : System.Web.UI.Page
{
    SqlConnection conn;
    DataSet ds;
    SqlCommand cmd;
    string sname, ssize, sdate, suser, snumb, sdetail, spath,sicon;
    public void Getinfo(int id)
    {
        conn = new SqlConnection(@"Server=DESKTOP-2OPG2CE\SQLEXPRESS;Database=Dataweb;Integrated Security=true");
        conn.Open();
        cmd = new SqlCommand("select * from Phanmem where ID = " + id + "", conn);
        SqlDataReader dr = cmd.ExecuteReader();
        if (dr.Read())
        {
            spath = dr[2].ToString();
            sname = dr[1].ToString();
            ssize = dr[3].ToString();
            sdate = dr[4].ToString();
            suser = dr[5].ToString();
            snumb = dr[6].ToString();
            sdetail = dr[7].ToString();
            sicon = dr[9].ToString();
            conn.Close();

        }
        conn.Close();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        int idapp = Convert.ToInt32(Request.QueryString["ID"]);
        Getinfo(idapp);
        //Session["PathSoft"] = dr[2].ToString();
        lbname.Text = sname;
        lbsize.Text = "Kích thước: " + ssize + " byte";
        lbdate.Text = "Updated: " + sdate;
        lbuser.Text = "Người tải: " + suser;
        lbnumb.Text = "Lượt tải xuống: " + snumb;
        lbdetail.Text = "Thông tin phầng mềm: " + sdetail;
        imgicon.ImageUrl = sicon;
    }
    protected void Button1_Click(object sender, EventArgs e)
    {

        int idapp = Convert.ToInt32(Request.QueryString["ID"]);
        Response.ContentType = "application/octect-stream";
        Response.AppendHeader("content-disposition", "filename=" + spath);
        Response.TransmitFile(Server.MapPath("~/Save/") + spath);
        conn = new SqlConnection(@"Server=DESKTOP-2OPG2CE\SQLEXPRESS;Database=Dataweb;Integrated Security=true");
        conn.Open();
        cmd = new SqlCommand("update Phanmem SET Download = Download +1 where ID = " + idapp + "", conn);
        cmd.ExecuteNonQuery();
    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        Response.Redirect("Home.aspx");
    }
}