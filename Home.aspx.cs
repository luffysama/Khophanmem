﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;

public partial class Home : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if ((int)Session["IsLogin"] != 0)
            {
                //Label2.Text = "Đã đăng nhập với tên " + Session["Fullname"].ToString();
                //btLogin1.Visible = false;
                //btLogout1.Visible = true;
                //btQli.Visible = true;
            }
        }
        catch
        {
            Session["IsLogin"] = 0;
            //btLogout.Visible = false;
            //btLogin.Visible = true;
        }
    }

    protected void btTimkiem_Click(object sender, EventArgs e)
    {
        SqlDataSource1.SelectCommand=("select * From Phanmem where  Viewname like N'%"+tbTimkiem.Text+"%'");
        Repeater1.DataBind();
        Label1.Text = "Kết  quả tìm kiếm: ";
        tbTimkiem.Text = "";
    }
    public static string ConvertToUnSign(string text)
    {
        for (int i = 33; i < 48; i++)
        {
            text = text.Replace(((char)i).ToString(), "");
        }

        for (int i = 58; i < 65; i++)
        {
            text = text.Replace(((char)i).ToString(), "");
        }

        for (int i = 91; i < 97; i++)
        {
            text = text.Replace(((char)i).ToString(), "");
        }
        for (int i = 123; i < 127; i++)
        {
            text = text.Replace(((char)i).ToString(), "");
        }
        text = text.Replace(" ", "");
        Regex regex = new Regex(@"\p{IsCombiningDiacriticalMarks}+");
        string strFormD = text.Normalize(System.Text.NormalizationForm.FormD);
        return regex.Replace(strFormD, String.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D');
    }
}