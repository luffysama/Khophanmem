﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Styles_MasterPage : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btHome_Click(object sender, EventArgs e)
    {
        Response.Redirect("Home.aspx");
    }
    
    protected void btLogout1_Click(object sender, EventArgs e)
    {
        Session["IsLogin"] = 0;
        Response.Redirect("Login.aspx");
    }
    protected void btLogin1_Click1(object sender, EventArgs e)
    {
        Response.Redirect("Login.aspx");
    }
    protected void Button4_Click(object sender, EventArgs e)
    {
        Response.Redirect("Upload.aspx");
    }
}
