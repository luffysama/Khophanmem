﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Home.aspx.cs" MasterPageFile="~/Styles/MasterPage.master" Inherits="Home" %>
<%@ Register assembly="DevExpress.Web.v16.1, Version=16.1.2.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="ajaxToolkit" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div style="height: auto">
        <asp:Label ID="Label2" runat="server"></asp:Label>
        <br />
        
        <asp:TextBox ID="tbTimkiem" runat="server" CssClass="TextBoxSearch" Height="33px" Width="544px"></asp:TextBox>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Button ID="btTimkiem" runat="server" OnClick="btTimkiem_Click" Text="Search me"  CssClass="button" />
        
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <br />
        <table>
            <tr>
                <td class="auto-style6"></td>
                <td>
        <asp:Label ID="Label1" runat="server"></asp:Label>
        
        <asp:Repeater ID="Repeater1" runat="server" DataSourceID="SqlDataSource1" >
            <HeaderTemplate>
                 <table>
                     <tr>
                         <th>       </th>
                         <th>       </th>
                     </tr>
            </HeaderTemplate>
            <ItemTemplate>
                    
                    <tr>
                        <td><a href="Chi_tiet.aspx?ID=<%# Eval("ID") %>"><%# Eval("Viewname")%></a></td>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%# Eval("Download")%></td>
                    </tr>
                
            </ItemTemplate>
            <SeparatorTemplate>
            <tr>
            <td colspan="20"><hr></td>
            </tr>
        </SeparatorTemplate>       
        <FooterTemplate>
        </table>
        </FooterTemplate>
        </asp:Repeater>
                </td></tr>
            </table>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:DatawebConnectionString %>" SelectCommand="SELECT * FROM [Phanmem] ORDER BY [Download] DESC, [ID]"></asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:DatawebConnectionString %>" SelectCommand="SELECT * FROM [Danhmuc]"></asp:SqlDataSource>
    </div>
</asp:Content>

<asp:Content ID="Content2" runat="server" contentplaceholderid="head">
    <style type="text/css">
        .auto-style6 {
            width: 67px;
        }
    </style>
    </asp:Content>

